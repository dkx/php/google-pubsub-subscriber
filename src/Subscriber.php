<?php

declare(strict_types=1);

namespace DKX\GooglePubSubSubscriber;

use Google\Cloud\PubSub\PubSubClient;
use Nette\Utils\Json;
use function call_user_func;
use function microtime;

final class Subscriber
{
	/** @var PubSubClient */
	private $client;

	/** @var string */
	private $subscriptionName;

	/** @var int */
	private $maxMessages = 100;

	/** @var int|null */
	private $maxSeconds;

	public function __construct(PubSubClient $client, string $subscriptionName)
	{
		$this->client = $client;
		$this->subscriptionName = $subscriptionName;
	}

	public function setMaxMessages(int $maxMessages): void
	{
		$this->maxMessages = $maxMessages;
	}

	public function setMaxSeconds(int $maxSeconds): void
	{
		$this->maxSeconds = $maxSeconds;
	}

	public function subscribe(callable $handler): void
	{
		$subscription = $this->client->subscription($this->subscriptionName);
		$startTime = microtime(true);
		$stopTime = null;

		if ($this->maxSeconds !== null) {
			$stopTime = $startTime + $this->maxSeconds;
		}

		while (true) {
			$messages = $subscription->pull([
				'maxMessages' => $this->maxMessages,
				'grpcOptions' => ['timeoutMillis' => null],
			]);

			foreach ($messages as $message) {
				$payload = Json::decode($message->data(), Json::FORCE_ARRAY);
				$state = call_user_func($handler, $payload, $subscription);

				if ($state === true) {
					$subscription->acknowledge($message);
					continue;
				}

				$subscription->modifyAckDeadline($message, 0);
			}

			if ($stopTime === null) {
				continue;
			}

			$now = microtime(true);

			if ($now >= $stopTime) {
				break;
			}
		}
	}
}
