<?php

declare(strict_types=1);

namespace DKX\GooglePubSubSubscriber;

use Google\Cloud\PubSub\Subscription;

interface SubscriptionInterface
{
	public function getConsoleName() : string;

	/**
	 * @param mixed[] $data
	 */
	public function processMessage(array $data, Subscription $subscription) : bool;
}
