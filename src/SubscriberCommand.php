<?php

declare(strict_types=1);

namespace DKX\GooglePubSubSubscriber;

use Exception;
use Google\Cloud\PubSub\PubSubClient;
use Google\Cloud\PubSub\Subscription;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use function is_string;

final class SubscriberCommand extends Command
{
	/** @var string */
	protected static $defaultName = 'pubsub:subscribe';

	/** @var PubSubClient */
	private $client;

	/** @var SubscriptionsManager */
	private $subscriptionsManager;

	public function __construct(PubSubClient $client, SubscriptionsManager $subscriptionsManager)
	{
		parent::__construct();

		$this->client = $client;
		$this->subscriptionsManager = $subscriptionsManager;
	}

	protected function configure(): void
	{
		$this->addArgument('console-name', InputArgument::REQUIRED, 'Subscription console name');
		$this->addArgument('subscription-name', InputArgument::REQUIRED, 'GCP subscription name');
		$this->addOption('max-seconds', null, InputOption::VALUE_OPTIONAL, 'Stop consumer after ? seconds');
		$this->addOption('max-messages', null, InputOption::VALUE_OPTIONAL, 'Number of messages to pull in each cycle');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$consoleName = $input->getArgument('console-name');
		if (!is_string($consoleName)) {
			throw new Exception('Missing console-name argument');
		}

		$subscriptionName = $input->getArgument('subscription-name');
		if (!is_string($subscriptionName)) {
			throw new Exception('Missing subscription-name argument');
		}

		$subscriptionImpl = $this->subscriptionsManager->getSubscriptionByConsoleName($consoleName);
		if ($subscriptionImpl === null) {
			throw new Exception('Subscription with console name "' . $consoleName . '" does not exists');
		}

		$subscriber = new Subscriber($this->client, $subscriptionName);

		$maxSeconds = $input->getOption('max-seconds');
		if (is_string($maxSeconds)) {
			$subscriber->setMaxSeconds((int) $maxSeconds);
		}

		$maxMessages = $input->getOption('max-messages');
		if (is_string($maxMessages)) {
			$subscriber->setMaxMessages((int) $maxMessages);
		}

		$subscriber->subscribe(function (array $data, Subscription $subscription) use ($subscriptionImpl) : bool {
			return $subscriptionImpl->processMessage($data, $subscription);
		});

		return 0;
	}
}
