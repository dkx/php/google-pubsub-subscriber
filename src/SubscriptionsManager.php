<?php

declare(strict_types=1);

namespace DKX\GooglePubSubSubscriber;

use function array_key_exists;

final class SubscriptionsManager
{
	/** @var SubscriptionInterface[] */
	private $consoleSubscriptions = [];

	public function addSubscription(SubscriptionInterface $subscription) : void
	{
		$this->consoleSubscriptions[$subscription->getConsoleName()] = $subscription;
	}

	public function getSubscriptionByConsoleName(string $consoleName) : ?SubscriptionInterface
	{
		if (array_key_exists($consoleName, $this->consoleSubscriptions)) {
			return $this->consoleSubscriptions[$consoleName];
		}

		return null;
	}
}
