# DKX/GooglePubSubSubscriber

Symfony console command for Google PubSub

## Installation

```bash
$ composer require dkx/google-pubsub-subscriber
```

## Usage

```php
<?php

use DKX\GooglePubSubSubscriber\SubscriberCommand;
use DKX\GooglePubSubSubscriber\SubscriptionInterface;
use DKX\GooglePubSubSubscriber\SubscriptionsManager;
use Google\Cloud\PubSub\PubSubClient;
use Symfony\Component\Console\Application;

final class TestSubscription implements SubscriptionInterface
{
    public function getConsoleName() : string
    {
        return 'my-test';    
    }

    public function processMessage(array $data) : bool
    {
        var_dump($data);
        return true;
    }
}

$client = new PubSubClient();

$manager = new SubscriptionsManager();
$manager->addSubscription(new TestSubscription());

$command = new SubscriberCommand($client, $manager);

$application = new Application();
$application->add($command);
$application->run();
```

and run:

```bash
$ php index.php pubsub:subscribe my-test my-app.prod.test --max-seconds 500 --max-messages 100
```

* `my-test`: string returned from your subscription's `getConsoleName()` method
* `my-app.prod.test`: name of existing subscription in Google Cloud Pub/Sub

## Standalone usage

```php
<?php

use DKX\GooglePubSubSubscriber\Subscriber;
use Google\Cloud\PubSub\PubSubClient;

$client = new PubSubClient();

$subscriber = new Subscriber($client, 'my-app.prod.test');
$subscriber->setMaxSeconds(500);
$subscriber->setMaxMessages(100);

$subscriber->subscribe(function (array $data): bool {
    var_dump($data);
    return true;
});
```
